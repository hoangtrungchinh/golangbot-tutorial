package main

import "fmt"

func main() {
	var age int = 27
	fmt.Println("My ages:", age)
	var name string
	name = "chinh"
	fmt.Println("My name = ", name)
	var heigh, weight int = 1700, 57000
	fmt.Println("My heigh is ", heigh, "my weight is ", weight)

	var (
		middle_name  = "Trung"
		lucky_number = 13
	)
	fmt.Println("Fullname: ", middle_name, "lucky_number: ", lucky_number)
	address, phoneNumber := "123 Sai Gon", 123456789

	fmt.Println("Address: ", address, "Phonumber: ", phoneNumber)
}
