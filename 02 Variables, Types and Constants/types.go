package main

import (
	"fmt"
	"unsafe"
)

func main() {
	a, b := true, false
	fmt.Println("a:", a, ", b:", b)
	fmt.Println("a && b:", a && b, ",a||b:", a || b)

	// int
	var c int = 90
	fmt.Printf("type of c is %T, size of c is %d", c, unsafe.Sizeof(c))

	// work with string
	name, middleName := "Chinh", "Hoang Trung"
	fmt.Println("\nFullname: ", middleName+" "+name)

	// convert type to be the same
	d := 1
	e := 1.5
	f := d + int(e)
	fmt.Println("d+e=", f)

}
